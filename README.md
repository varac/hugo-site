# Hugo website

* Production: <https://www.varac.net>
* Staging (main): <https://www-staging.oas.varac.net/>

## Theme

* <https://github.com/samrobbins85/hugo-developer-portfolio>

## Todo

* Run `hugo` as pre-commit hook
* Add robots.txt
* Redesign blog
* Portfolio:
  * Re-use same links for same tools
* Imprint ?
* Add bottom logo to 0xacab
* Footer
  * Fix Adress popup
  * git sha

## Etc

<https://gohugo.io/>

* [Repo](https://0xacab.org/varac/hugo-site)

### New post

    hugo new post/weechat.md

### Manual deploy (old, deprecated method)

Generate `public/` dir

    hugo

Generate with drafts included

    hugo -D

Run local server (optional)

    hugo server [-D]

# Static files

see `~/.gnupg/fingerprints/README.md`
