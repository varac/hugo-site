# Hugo website

- Production: <https://www.varac.net>
- Staging (main): <https://www-staging.k.varac.net/>

## Theme

- <https://github.com/samrobbins85/hugo-developer-portfolio>

## Todo

- Don't update lastmod timestamp on every page on every commit
  - Run `hugo` as pre-commit hook
- Add robots.txt
- Redesign blog
- Portfolio:
  - Re-use same links for same tools
- Imprint ?
- Add bottom logo to 0xacab
- Footer
  - Fix Adress popup
  - git sha

## Etc

<https://gohugo.io/>

- [Repo](https://0xacab.org/varac/hugo-site)

### New post

```sh
hugo new post/weechat.md
```

### Manual deploy (old, deprecated method)

Generate `public/` dir

```sh
hugo
```

Generate with drafts included

```sh
hugo -D
```

Run local server (optional)

```sh
hugo server [-D]
```

## Static files

see `~/.gnupg/fingerprints/README.md`
