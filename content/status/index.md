---
title: "Status"
tags: ['status']
---
Last commit: {{ .GitInfo.AbbreviatedHash }} {{.GitInfo.CommitDate}}

Last Update:
<time datetime="{{ .Page.Lastmod.Format "Mon Jan 10 17:13:38 2020 -0700" }}" class="text-muted">
  {{ $.Page.Lastmod.Format "January 02, 2006" }}
</time>

{{- if .Lastmod }}
    {{- $lastmod := .Lastmod.Format "02.01.2006" }}
    {{- if ne $lastmod $pubdate }}
        <div class="post-info-last-mod">
            (Updated:
            <time datetime="{{ .Lastmod }}" title="{{ .Lastmod }}">
                {{ $lastmod }}
            </time>)
        </div>
    {{- end }}
{{- end }}
