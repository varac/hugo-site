---
title: "Contact"
tags: ["varac", "contact", "email"]
---

- Email:
  {{< rawhtml >}}
  <A HREF="mailto:&#118;&#097;&#114;&#097;&#099;&#064;&#118;&#097;&#114;&#097;&#099;&#046;&#110;&#101;&#116;">Drop me a mail</A>.
  {{< /rawhtml >}}
  - Here's my [OpenPGP key](https://keys.openpgp.org/vks/v1/by-fingerprint/4AFAAA882998AC891E7BDD4D5465E77E7876ED04), for fingerprint verification see [keys.txt](../keys.txt).
- Matrix: `<my nickname>@systemli.org`
- Signal: Please ask me for phone number
