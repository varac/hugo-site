+++
title = "Centro de autonomía digital"
categories = ["sysadmin"]
coders = ""
description = "System administration."
github = ["https://github.com/digitalautonomy"]
site = "https://autonomia.digital"
image = "https://avatars2.githubusercontent.com/u/30272304?s=200&v=4"
[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/6/67/Kubernetes_logo.svg"
name = "Kubernetes"
url = "https://kubernetes.io"
[[tech]]
logo = "../../img/tools/ansible.png"
name = "Ansible"
url = "https://www.ansible.com"
[[tech]]
logo = "https://www.puppet.com/sites/default/themes/custom/puppet/logo.svg"
name = "Puppet"
url = "https://puppet.com"
[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg"
name = "Docker"
url = "https://www.docker.com"
#[[tech]]
#logo = "https://avatars0.githubusercontent.com/u/32228528?s=200&v=4"
#name = "CoyIM"
#url = "https://github.com/coyim/coyim"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Icinga_logo.svg/250px-Icinga_logo.svg.png"
name = "Icinga"
url = "https://icinga.com"

+++

I worked as a sysadmin for CAD during 2017-2018.
