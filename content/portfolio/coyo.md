+++
title = "Coyo App"
categories = ["kubernetes"]
coders = ""
description = "Coyo ist eine integrierte Social-Intranet-Software, die für die Unternehmenskommunikation, Kollaboration und Team Messaging genutzt wird. COYO verbindet die Konzepte von klassischem Intranet und von Social Networks miteinander."
github = ["https://github.com/coyoapp"]
site = "https://www.coyoapp.com/"
image = "https://avatars.githubusercontent.com/u/38757776?s=200&v=4"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/6/67/Kubernetes_logo.svg"
name = "Kubernetes"
url = "https://kubernetes.io"

[[tech]]
logo = "https://boxboat.com/2020/02/04/writing-a-custom-terraform-provider/featured.png"
name = "Terraform"
url = "https://www.terraform.io/"

[[tech]]
name = "Prometheus"
logo = "https://cncf-branding.netlify.app/img/projects/prometheus/horizontal/color/prometheus-horizontal-color.png"
url = "https://prometheus.io"

# Todo
#
# * datadog
# *  terratest
# * docker
# * OpenTelekomCloud
# * Helm


#[[tech]]
#logo = ""
#name = ""
#url = ""


+++

Freelancer for kubernetes migration from 2020-10 until 2021-03.
