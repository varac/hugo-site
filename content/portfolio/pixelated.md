+++
title = "Pixelated"
categories = ["email"]
coders = ""
description = "Modern email with privacy."
github = ["https://github.com/pixelated"]
site = "https://pixelated.github.io/"
image = "https://pixelated.github.io/assets/images/pixelated-logo.svg"

[[tech]]
logo = "https://www.vizteams.com/wp-content/uploads/2013/08/python-logo-master.png"
name = "Python"
url = "https://www.python.org"

[[tech]]
logo = "https://puppet.com/images/logos/puppet-logo-black.svg"
name = "Puppet"
url = "https://puppet.com"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg"
name = "Docker"
url = "https://www.docker.com"

[[tech]]
name = "AWS EC2"
logo = "https://server-network-note.net/wp-content/uploads/2017/11/AWS-EC2-00.png"
url = "https://aws.amazon.com/en/ec2/"

[[tech]]
logo = "https://leap.se/img/hero-image.png"
# logo = "../../img/tools/leap.png"
name = "LEAP"
url = "../../portfolio/leap"

[[tech]]
logo = "https://www.gnupg.org/share/logo-gnupg-light-purple-bg.png"
name = "GnuPG"
url = "https://www.gnupg.org"

[[tech]]
logo = "https://flightjs.github.io/img/flight-logo.png"
name = "FlightJS"
url = "https://flightjs.github.io"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png"
name = "ReactJS"
url = "https://reactjs.org/"
+++

I worked on this open source project for [Thoughtworks](https://www.thoughtworks.com) during 2014-2017
