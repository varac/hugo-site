+++
title = "Stackspin"
categories = ["kubernetes" ,"office"]
coders = ""
description = "Stackspin (formerly OpenAppStack) is an online office suite in which you control the data. It includes everything a small organisation needs to get themselves organised. Partly funded by the german Prototype Fund."
github = ["https://open.greenhost.net/groups/stackspin"]
site = "https://www.stackspin.net"
image = "https://www.stackspin.net/wp-content/uploads/2022/02/logo.png"

[[tech]]
logo = "https://prototypefund.de/wp-content/uploads/2025/01/4a84c2927c16d8bb69d6eb2e0b66e14a_user-svg_2025-01-08_02-19-31-1.svg"
name = "Prototype Fund"
url = "https://prototypefund.de/project/kollaborative-apps-fuer-openappstack/"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/6/67/Kubernetes_logo.svg"
name = "Kubernetes"
url = "https://kubernetes.io"

[[tech]]
logo = "https://k3s.io/img/k3s-logo-light.svg"
name = "k3s"
url = "https://k3s.io"

[[tech]]
logo = "../../img/tools/ansible.png"
name = "Ansible"
url = "https://www.ansible.com"

[[tech]]
logo = "../../img/tools/flux.png"
name = "Flux"
url = "https://fluxcd.io"

#[[tech]]
#logo = "../../img/tools/velero.svg"
#name = "Velero"
#url = "https://velero.io"

[[tech]]
logo = "https://cdn-images-1.medium.com/max/800/1*IvCDlfi3vQfgyKO1eFv4jA.png"
name = "GraphQL"
url = "https://graphql.org"

[[tech]]
logo = "../../img/tools/hydra.png"
name = "Hydra"
url = "https://www.ory.sh/hydra/"

[[tech]]
name = "Prometheus"
logo = "https://avatars.githubusercontent.com/u/3380462?s=200&v=4"
url = "https://prometheus.io"

[[tech]]
name = "Grafana"
logo = "https://avatars.githubusercontent.com/u/7195757?s=200&v=4"
url = "https://grafana.com"

#[[tech]]
#logo = ""
#name = ""
#url = ""


+++

I work on this open source project for [Greenhost](https://greenhost.net/)
since 2019.
