+++
title = "Stackspin"
categories = ["kubernetes" ,"office"]
coders = ""
description = "An online office suite in which you control the data. It includes everything a small organisation needs to get themselves organised."
github = ["https://open.greenhost.net/groups/stackspin"]
site = "https://stackspin.net"
image = "https://www.stackspin.net/wp-content/uploads/2022/02/logo.png"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/6/67/Kubernetes_logo.svg"
name = "Kubernetes"
url = "https://kubernetes.io"

[[tech]]
logo = "https://k3s.io/img/k3s-logo-light.svg"
name = "k3s"
url = "https://k3s.io"

[[tech]]
logo = "../../img/tools/ansible.png"
name = "Ansible"
url = "https://www.ansible.com"

[[tech]]
logo = "../../img/tools/flux.png"
name = "Flux"
url = "https://fluxcd.io"

#[[tech]]
#logo = "../../img/tools/velero.svg"
#name = "Velero"
#url = "https://velero.io"

[[tech]]
logo = "https://flask.palletsprojects.com/en/1.1.x/_images/flask-logo.png"
name = "Python Flask"
url = "https://palletsprojects.com/p/flask/"

[[tech]]
logo = "https://camo.githubusercontent.com/4aa5532ee9baf623c95b901372002dfa4e97ff01/687474703a2f2f696d6775722e636f6d2f56344c746f49492e706e67"
name = "NuxtJS"
url = "https://nuxtjs.org/"

[[tech]]
logo = "https://cdn-images-1.medium.com/max/800/1*IvCDlfi3vQfgyKO1eFv4jA.png"
name = "GraphQL"
url = "https://graphql.org"

[[tech]]
logo = "../../img/tools/hydra.png"
name = "Hydra"
url = "https://www.ory.sh/hydra/"

[[tech]]
name = "Prometheus"
logo = "https://cncf-branding.netlify.app/img/projects/prometheus/horizontal/color/prometheus-horizontal-color.png"
url = "https://prometheus.io"

[[tech]]
name = "Grafana"
logo = "https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/Grafana_logo.svg/63px-Grafana_logo.svg.png"
url = "https://grafana.com"

#[[tech]]
#logo = ""
#name = ""
#url = ""


+++

I work on this open source project for [Greenhost](https://greenhost.net/)
since 2019.
