+++
title = "Norddeutscher Rundfunk"
categories = ["kubernetes" ,"gcp"]
coders = ""
description = "Cloud engineer at the NDR, a regional public radio and television broadcaster. Administration of major german news sites."
github = ["https://github.com/NorddeutscherRundfunk"]
site = "https://ndr.de"
image = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/NDR_Dachmarke.svg/2560px-NDR_Dachmarke.svg.png"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/6/67/Kubernetes_logo.svg"
name = "Kubernetes"
url = "https://kubernetes.io"

[[tech]]
logo = "../../img/tools/ansible.png"
name = "Ansible"
url = "https://www.ansible.com"

[[tech]]
name = "Prometheus"
logo = "https://avatars.githubusercontent.com/u/3380462?s=200&v=4"
url = "https://prometheus.io"

[[tech]]
name = "Grafana"
logo = "https://avatars.githubusercontent.com/u/7195757?s=200&v=4"
url = "https://grafana.com"

#[[tech]]
#logo = ""
#name = ""
#url = ""


+++
