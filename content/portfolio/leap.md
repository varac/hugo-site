+++
categories = ["email", "vpn", "platform" ]
coders = ""
description = "LEAP is a non-profit dedicated to giving all internet users access to secure communication. Our focus is on adapting encryption technology to make it easy to use and widely available."
github = ["https://github.com/leapcode"]
site = "https://leap.se"
image = "https://leap.se/images/leap.svg"
title = "LEAP Encryption Access Project"

[[tech]]
logo = "https://www.puppet.com/sites/default/themes/custom/puppet/logo.svg"
name = "Puppet"
url = "https://puppet.com"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg"
name = "Docker"
url = "https://www.docker.com"

[[tech]]
name = "AWS EC2"
logo = "https://server-network-note.net/wp-content/uploads/2017/11/AWS-EC2-00.png"
url = "https://aws.amazon.com/en/ec2/"

[[tech]]
logo = "https://www.gnupg.org/share/logo-gnupg-light-purple-bg.png"
name = "GnuPG"
url = "https://www.gnupg.org"

[[tech]]
logo = "http://boxchronicles.com/wp-content/uploads/2015/01/ruby.png"
name = "Ruby"
url = "https://www.ruby-lang.org/"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Ruby_On_Rails_Logo.svg/220px-Ruby_On_Rails_Logo.svg.png"
name = "Ruby on rails"
url = "https://rubyonrails.org/"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Apache_CouchDB_logo.svg/330px-Apache_CouchDB_logo.svg.png"
name = "CouchDB"
url = "https://couchdb.apache.org/"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Tor-logo-2011-flat.svg/220px-Tor-logo-2011-flat.svg.png"
name = "Tor"
url = "https://www.torproject.org"

[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/f/f5/OpenVPN_logo.svg"
[[tech]]
logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Icinga_logo.svg/250px-Icinga_logo.svg.png"
name = "Icinga"
url = "https://icinga.com"

#[[tech]]
#logo = ""
#name = ""
#url = ""


+++

I worked on this open source project during 2011-2014.
