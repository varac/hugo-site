---
title: "About me"
tags: ['about', 'me', 'varac', 'profile', 'skills']
---

I'm a freelancing system engineer, inspired by DevOps and passionate about open source.

See the [projects](../portfolio) I have been working on in the last years, or
[contact me](../contact) !

{{< figure src="/img/skills.wortwolken.com.png" title="What's on my work mind" >}}
