---
title: "Tmate"
description: "Sharing your local terminal with trusted peers."
date: 2020-09-23
tags: ["remote-work"]
type: post
---

Over the past few years I worked different remote setups. I often miss the [pair
programming](https://en.wikipedia.org/wiki/Pair_programming) fun from my time
at [Thoughtworks](https://www.thoughtworks.com/).
There are numerous tools you can use for remote pair programming, or a quick
get together to work on an issue that is better investigated together.
A colleague of mine wrote a blog post [how to use a remote tmux session for pair programming](https://www.hamvocke.com/blog/remote-pair-programming-with-tmux/)
a while ago. I recommed this article because it covers most of the basics
of what I would need to elaborate here.
This is great if you meet together on a dedicated, remote server
(with ssh and tmux installed), but what if you just want to share your beloved
local dev environment, which your favorite text editor, shell aliases and more
with your colleague(s) ? There no place like localhost !

## Tmate

That's where [tmate](https://tmate.io/) comes to the rescue.
Tmate is a fork of tmux which lets you invite others to join your terminal, just
like a remote tmux session but on your very own device.
It consist of a tmate binary which is used as the client to connect to a shared
session, and [tmate-ssh-server](https://github.com/tmate-io/tmate-ssh-server),
which connects the clients together.
tmate.io runs a free service which you can use to easily share your terminal.
However, it's a centralized service and I don't feel too comfortable sharing one server
with possibly hundreds of others while sharing my terminal.

## History of tmate-ssh-server

But `tmate-ssh-server` is free software so you can host it yourself!
Another colleague of mine added support for [limiting trusted ssh client via an
authorized keys file](https://github.com/tmate-io/tmate-ssh-server/pull/38), so
you could self-host `tmate-ssh-server` in a controlled and trusted environment.
Unfortunatly, this feature was
[taken out](https://github.com/tmate-io/tmate-ssh-server/commit/48884c95c994c7bff01dee24f2230e1263db2f85)
again during a refactoring, and there is a pending [feature request to re-add
it](https://github.com/tmate-io/tmate-ssh-server/issues/68).
If you want to continue running your own `tmate-ssh-server` you need to compile
a particular version of it. On top of that, you also need to use [tmate 2.3.1](https://github.com/tmate-io/tmate/releases/tag/2.3.1)
(static binaries are linked as assets in the github release page).

That's a pretty nasty state because you can't easily patch security issues etc.
However, I still prefer this over the centralized service of tmate.io.
