---
title: 'Permanent remote IRC session with tmux and weechat'
description: 'Never miss a message again.'
date: 2018-01-28T12:19:57+01:00
image: "https://www.tecmint.com/wp-content/uploads/2016/06/WeeChat-IRC-Client-for-Linux.png"
tags: ['irc','weechat', 'remote-work', 'tmux']
type: "post"
---

[Internet Relay Chat (IRC)](https://en.wikipedia.org/wiki/Internet_Relay_Chat)
is a popular chat protocol that is widely used in
[FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) development.
But different timezones and the fact that not everyone is online 24/7
makes communication hard when you have a setup that forces you to quit
and re-join the chat frequently (when shutting down your laptop, moving
to another location etc.)

It doesn't make sense to enter a room, ask a question, and leave 3 hours
after, before anyone could answer because they are located on the other
side of the world and are just waking up.

The solution is a permanent remote IRC session which allows you to have a backlog
for times where you're not actively behind your laptop.

# Bouncer, remote shell or Matrix

There are different methods how you can achieve this.

- [IRC-Bouncer](https://en.wikipedia.org/wiki/BNC_(software)#IRC)
  is a software that runs on a remote server and acts like a proxy in
  between your local (desktop) client and the irc server.
- [Matrix](https://en.wikipedia.org/wiki/Matrix_(communication_protocol))
  is a different, federated communication protocol which lets you allow to bridge
  other chat protocols (like IRC).
- A long-running shell on a remote server running [weechat in relay mode](https://weechat.org/files/doc/stable/weechat_user.en.html#relay_plugin),
  which can either act as `IRC-Bouncer` itself, or can serve
  [weechat protocal compatible clients](https://weechat.org/files/doc/stable/weechat_user.en.html#relay_plugin)
  (Android, cross-platform GUI client and Web-based client).
- A long-running shell on a remote server which allows you to run a terminal-based
  IRC client, which you attach to when you're online.

Even `matrix` is very promising, I have encountered severe communication issues when
ppl used a [matrix-irc bridge](https://github.com/matrix-org/matrix-appservice-irc)
enabled server, i.e. messages I sent to them were never recieved, which fucks up
communication pretty badly.

I'll focus on the last option here, simply it works for me pretty well.
If you prefer a GUI IRC dektop client, either the `IRC-Bouncer` or the `weechat relay`
would be a better option for you.

# Prerequisites

A shell on a remote server, with following tools installed:

- A so-called "terminal multiplexer" which lets you re-attach a running
  shell session, like [tmux](https://en.wikipedia.org/wiki/Tmux) or [GNU screen](https://en.wikipedia.org/wiki/GNU_Screen)
- A chat client with a text user interface, like [weechat](https://weechat.org/)
  or [irssi](https://irssi.org/)
  (see also [Comparison of Internet Relay Chat clients](https://en.wikipedia.org/wiki/Comparison_of_Internet_Relay_Chat_clients))

I'll explain the `weechat` and `tmux` combination here, but this is mainly
personal preference.

# Getting around in tmux

Before i reinvent the wheel, here's a [good tutorial for tmux](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-tmux-on-ubuntu-12-10--2).

Of course there are dozens of other howtos out there.

We won't need the complexity of multiple windows and other functionallity
that tmux brings - for this setup, we only use the ability to attach
and detach to a running session.

The very first time we need to initiallaly start a tmux session

```
# tmux
```

Now you can do stuff in newly started shell. Use `Ctrl-b d` to detach the session.

You can re-attach the shell with:

```
# tmux attach
```

Now continue doing stuff on running shell, and use `Ctrl-b d` again
to detach the session.

If you're familiar with tmux, you can move on to the next topic.

# Weechat

`Irssi` was the first text based chat client, released in 1999.
`Weechat` followed a few years after. Both offer mainly similar
functionallity.
Don't confuse `weechat` with [wechat](https://www.wechat.com/en/) !

I prefer weechat, mainly because it is in active development
(where irssi recieving only bugfixes) and comes with a nice
nicklist bar that displayes well in mobile devices as well.

Here's the [oficial weechat quick start guide](http://weechat.org/files/doc/devel/weechat_quickstart.en.html).

Start weechat with

```
# weechat
```

In this example we'll join the `#leap` chat is on the
[freenode IRC network](How can I connect to freenode server using SSL?).

`freenode IRC network` is pre-configuerd in weechat, but we still need
to configure SSL for it, please follow
[How can I connect to freenode server using SSL?](https://weechat.org/files/doc/devel/weechat_faq.en.html#irc_ssl_freenode)
to set it up.

Then save the settings

```
/save
```

Connect to it with

```
/connect freenode
```

and join #leap with

```
/join #leap
```

And tomorrow ?

The main daily task from now will be to login to the server, attach
and attach to the tmux session. You don't need to properly detach from
it, just shutting down your laptop will do this as well.

```
# tmux attach
```

The running weechat session wil show up, remember to use `Ctrl-b d`
to detach the session.

Then exit from your login shell:

```
# exit
```

Have fun !
