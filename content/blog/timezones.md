---
title: "Timezones"
description: "Can we please get rid of them entirely ? ☺"
date: 2018-02-23T10:59:20+01:00
tags: ["remote-work", "timezones"]
type: post
---

When you work in a distributed team sometimes it's hard to find overlapping
hours that you can actually work together. Don't get me even started on the daylight saving
madness, when all countries your collegues work in change DST _at different moments_ !

Here are a few small tools that make my life easier.

## Displaying local times in differnt timezones

It's as easy as this:

```sh
$ echo "Bogota: $(TZ=America/Bogota date)"
Bogota: Fri Feb 23 05:32:36 -05 2018
```

I use this little script to have an overview of local times in different locations:

```sh
#!/bin/sh
#
alias datetime="date +%H:%M"
echo "What's the LEAP UNIVERSAL TIME?"
echo '-------------------------------'
echo 'sf (PST):\t'  `TZ="America/Los_Angeles" datetime`
echo 'ny (EST):\t'  `TZ="America/New_York" datetime`
echo 'Sao Paulo:\t' `TZ="America/Sao_Paulo" datetime`
echo 'UTC:\t\t'     `TZ="UTC" datetime`
echo 'berlin:\t\t'  `TZ="Europe/Berlin" datetime`
```

This is an example output:

```sh
What's the LEAP UNIVERSAL TIME?
-------------------------------
sf (PST):  02:02
ny (EST):  05:02
Sao Paulo:  07:02
UTC:   10:02
berlin:   11:02
```

## Undertime

[Undertime](https://gitlab.com/anarcat/undertime/) is a neat
python program that shows the different local times next to each other grouped by
their timezone. It makes finding overlap time (i.e. for scheduling meetings) easy.
Here's an example output, [this undertime screenshot](https://gitlab.com/anarcat/undertime/raw/main/undertime.png)
gives you a better idea how it colors the overlap for better visibility.

```sh
» undertime Los_Angeles EST Bogota New_York Sao_Paulo UTC London Europe/Berlin
WARNING:root:skipping zone America/Bogota with existing offset -5:00:00
WARNING:root:skipping zone America/New_York with existing offset -5:00:00
WARNING:root:skipping zone Europe/London with existing offset 0:00:00
WARNING:root:skipping zone Europe/Berlin with existing offset 1:00:00
┌───────┬─────────────────────┬───────┬───────────────────┬───────┐
│  CET  │ America/Los_Angeles │  EST  │ America/Sao_Paulo │  UTC  │
├───────┼─────────────────────┼───────┼───────────────────┼───────┤
│ 00:00 │        15:00        │ 18:00 │       20:00       │ 23:00 │
│ 01:00 │        16:00        │ 19:00 │       21:00       │ 00:00 │
│ 02:00 │        17:00        │ 20:00 │       22:00       │ 01:00 │
│ 03:00 │        18:00        │ 21:00 │       23:00       │ 02:00 │
│ 04:00 │        19:00        │ 22:00 │       00:00       │ 03:00 │
…
```
