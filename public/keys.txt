-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

This is a list of fingerprints for different accounts.
You can verify it if you already have imported and verified my gpg key by:

    wget https://www.varac.net/keys.txt
    gpg --verify keys.txt

GPG
===

Get my key either with browsing to

    https://keys.openpgp.org/vks/v1/by-fingerprint/4AFAAA882998AC891E7BDD4D5465E77E7876ED04

or fetch it with the gpg cli tool:

    gpg --search-keys 4AFAAA882998AC891E7BDD4D5465E77E7876ED04

The fingerprint is:

    4AFA AA88 2998 AC89 1E7B  DD4D 5465 E77E 7876 ED04

OTR
===

  IRC:  varac@chat.freenode.net                   5331EF72 0A190BBA B91A2A80 5EEC5303 D16D814E
  IRC:  varac@irc.oftc.net                        5331EF72 0A190BBA B91A2A80 5EEC5303 D16D814E
  IRC:  varac@irc.indymedia.org                   5331EF72 0A190BBA B91A2A80 5EEC5303 D16D814E

  XMPP: varac@jabber.systemli.org/pidgin          7BBFECF2 9714E499 9C24D5D2 6E451866 A6105E22
  XMPP: varac@jabber.systemli.org/profanity       5A9935C7 A19CF046 E294D828 900A3014 F3D43507

Signal
======

My fingerprint for Signal is:                    20803 15988 38769 31833 20340 89230

It shows up in the conversation security number either as the first 6 chunks or the last 6 chunks.
See https://signal.org/blog/safety-number-updates/ for details.

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEESvqqiCmYrIkee91NVGXnfnh27QQFAl+yhwEACgkQVGXnfnh2
7QR+/w/+LPuj+kzlc1W82anGvzczHUy8oxetyWAvCJlfSAAP1eqZpi8tJbV1TytC
WRal9eugxF+ruHrrFu0p5hRxva6ZZGpaENBC+AYnmSbMzKLwKe28W6fh/JkD4Ei1
SGSJN8ldrWj/e/ClRcoFbug/LT91w5ZVj36Z3B/6eC5M+Y03J/NgSp/a2/Ya0zjH
oPzmtKVBgre7rlxaedsEXYsj+RSrmTnTvCGCmYt0uDd7OFtwIjwsxhF0HJd5JqlG
Pa0Cmv7RZq0FO77ikl62rTe2Q5Lm5qLG9NDpi8rCcGeekWtS6Igws50+C4QOAogw
MorOAo5lX286oZxmw6S98iuM/4t37xFiJZ5Iyg4dKCnYB8TRZIg3nm06mc2fdUyT
2h6C/h7uWOZvrfsqDeg9Osxgzcdh89r82LZWWCKq20kniKbYJKu2IncEs36f+2cQ
4vf+/IMC3dtpVz53Nev0qw5IZrdG2c2vQ+Ga96ucoNWiOnsWPqBzGzqn1idX8V6i
eTIIFsgJz/b8tLI0sn28rZQwjblgMuvrrDYyoijTPdnbRiQptMrnQ0qHQiOsYrrl
Oukir0F7dg8wUtFQIJuMZabVmSzO/tEmKYnOBET4uAeG6wRFnf+fy1/x7GtT+q3/
nXcRt07VZKqz3x1IFCNgwCrBT25jgmNREKvTqWDP8jHELZcWod8=
=3anW
-----END PGP SIGNATURE-----
